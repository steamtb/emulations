#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {

  struct addrinfo hints, *server_info=NULL, *server=NULL;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;          //ipv4
  hints.ai_socktype = SOCK_DGRAM;     //udp
  hints.ai_flags = AI_NUMERICSERV;    //use numeric port number
  const char *port_num = "4747";

  if( getaddrinfo("b", port_num, &hints, &server_info) != 0 ) {
    printf("could not get addrinfo for b\n");
    exit(1);
  }

  int fd;
  for(server = server_info; server != NULL; server = server->ai_next) {
    fd = socket(server->ai_family, server->ai_socktype, server->ai_protocol);
    if(fd == -1) {
      continue;
    }
    break;
  }

  if(server == NULL) {
    printf("could not reslove server b\n");
    exit(1);
  }

  freeaddrinfo(server_info);

  const char* message = "muffin";
  size_t sz = strlen(message);
  size_t n = sendto(fd, message, sz, 0, server->ai_addr, sizeof(*server->ai_addr));
  if(n < sz) 
  {
    printf("short write :(\n");
  }

  printf("muffin sent :)\n");

  close(fd);
  freeaddrinfo(server);

  return 0;

}
