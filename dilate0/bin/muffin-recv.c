#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {

  struct addrinfo hints, *server_info=NULL, *server=NULL;
  memset(&hints, 0, sizeof hints);
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_family = AF_INET;
  hints. ai_flags = AI_PASSIVE | AI_NUMERICSERV;
  const char *port_num = "4747";
  
  if( getaddrinfo(NULL, port_num, &hints, &server_info) != 0 ) {
    printf("could not get addrinfo for myself\n");
    exit(1);
  }

  int lfd;
  for(server = server_info; server != NULL; server = server->ai_next) {
    lfd = socket(server->ai_family, server->ai_socktype, server->ai_protocol);
    if(lfd == -1) {
      continue;
    }
    int opt = 1;
    if(setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
      printf("could not set REUSEADDR\n");
      exit(1);
    }
    if(bind(lfd, server->ai_addr, server->ai_addrlen) == 0) {
      break;
    }

    // if we're here, bind did not succeed
    close(lfd);
  }

  if(server == NULL) {
    printf("could bind to address b\n");
    exit(1);
  }

  freeaddrinfo(server_info);

  for (;;) {
    char buf[64];
    memset(buf, 0, sizeof(buf));
    if(recvfrom(lfd, buf, sizeof(buf), 0, NULL, NULL) < 0) {
      printf("read error\n");
      exit(1);
    }

    printf("read: %s\n", buf);
  }

  return 0;

}
