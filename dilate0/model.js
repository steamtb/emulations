
a = node('a');
b = node('b');
s = {name: 's', image: 'cumulusvx-3.5-mvrf'};

topo = {
  name: 'dilate0',
  nodes: [a, b],
  switches: [s],
  links: [
    Link('a', 1, 's', 1),
    Link('b', 1, 's', 2)
  ]
};

function node(name) {
  return {
    name: name,
    image: 'fedora-27',
    cmdline: 'root=/dev/sda1 rw',
    kernel: 'bzImage',
    defaultnic: 'e1000',
    defaultdisktype: { dev: 'sd', bus: 'sata' },
    mounts: [{
      source: env.PWD+'/bin',
      point: '/usr/local/bin'
    }]
  };
}
