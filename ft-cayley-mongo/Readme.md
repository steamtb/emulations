# Fattree Cayley Mongo

This folder contains an emulation of a Fattree network that is host to a Cayley graph database backed by a sharded MongoDB setup.

![Alt text](./ft-cayley-mongo.svg)
