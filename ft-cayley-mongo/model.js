
bluespines = Range(2).map(i => cumulus('bluespine', i));
greenspines = Range(2).map(i => cumulus('greenspine', i));

bluefabrics = Range(2).map(i => cumulus('bluefabric', i));
greenfabrics = Range(2).map(i => cumulus('greenfabric', i));

tors = Range(8).map(i => cumulus('tor', i));

nodes = Range(32).map(i => server('n', i));

ports = {
  bluespine: [1, 1],
  greenspine: [1, 1],
  bluefabric: [1, 1],
  greenfabric: [1, 1],
  tor: [1, 1, 1, 1, 1, 1, 1, 1]
}

topo = {
  name: 'ft-cayley-mongo',
  nodes: nodes,
  switches: [...bluespines, ...greenspines, ...bluefabrics, ...greenfabrics, ...tors],
  links: [
    ...greenfabrics.map((x,i) => Link(
      x.name, ports.greenfabric[i]++, 'greenspine0', ports.greenspine[i%2]++)),
    ...greenfabrics.map((x,i) => Link(
      x.name, ports.greenfabric[i]++, 'greenspine1', ports.greenspine[i%2]++)),

    ...bluefabrics.map((x,i) => Link(
      x.name, ports.bluefabric[i]++, 'bluespine0', ports.bluespine[i%2]++)),
    ...bluefabrics.map((x,i) => Link(
      x.name, ports.bluefabric[i]++, 'bluespine1', ports.bluespine[i%2]++)),

    ...tors.map((x,i) => Link(
      x.name, ports.tor[i]++, 'bluefabric'+(Math.floor(i/4)), ports.bluefabric[Math.floor(i/4)]++)), 

    ...tors.map((x,i) => Link(
      x.name, ports.tor[i]++, 'greenfabric'+(Math.floor(i/4)), ports.greenfabric[Math.floor(1/4)]++)), 

    ...nodes.map((x,i) => Link(
      x.name, 1, 'tor'+(Math.floor(i/4)), ports.tor[Math.floor(i/4)]++))
  ]
}

function server(name, index) {
  return {
    'name': name + index,
    'image': 'fedora-27',
    'os': 'linux',
    'cpu': { 'cores': 4 },
    'memory': { 'capacity': GB(4) },
    'mounts': [ { 'source': env.PWD+'/node', 'point': '/tmp/config' } ]
  };
}

function cumulus(name, index) {
  return {
    'name': name + index,
    'image': 'cumulusvx-3.5-mvrf',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(1) },
    'mounts': [ { 'source': env.PWD+'/switch', 'point': '/tmp/config' } ]
  };
}
