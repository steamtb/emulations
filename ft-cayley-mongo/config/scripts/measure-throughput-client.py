#!/usr/bin/python3

import subprocess, os, sys, requests, socket, json

server = os.getenv('SERVER', 'influxdb')
db = os.getenv('DB', 'perfdb')
me = socket.gethostname()

if len(sys.argv) < 2:
    print('usage: measure-throughput-client.py <server>')
    sys.exit(1)

target = sys.argv[1]

p = subprocess.run(
        args = ['iperf3', '--json', '--client', target],
        stdout = subprocess.PIPE)

if p.returncode != 0:
    print('error running iperf')
    sys.exit(1)


try:
    data = json.loads(p.stdout)
    bps = data['end']['streams'][0]['sender']['bits_per_second']
    max_rtt = data['end']['streams'][0]['sender']['max_rtt']
    min_rtt = data['end']['streams'][0]['sender']['min_rtt']
    mean_rtt = data['end']['streams'][0]['sender']['mean_rtt']
except:
    print('iperf output does not contain path expected information')
    print(data)
    sys.exit(1)

r = requests.post(
        "http://%s:8086/write?db=%s"%(server,db), 
        data="iperf,from=%s,to=%s bps=%f,max_rtt=%f,min_rtt=%f,mean_rtt=%f"%(
            me, target, bps, max_rtt, min_rtt, mean_rtt)
        )
print(r.status_code, r.reason)

