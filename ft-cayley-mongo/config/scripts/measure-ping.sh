#!/bin/bash

COUNT=${COUNT:=10}
SERVER=${SERVER:=influxdb}
DB=${DB:=perfdb}
me=`hostname`
url="http://$SERVER:8086"

if [[ $# -ne 1 ]]; then
  echo "usage: measure-ping.sh <to-address>"
  exit 1
fi

target=$1

out=`ping -c $COUNT -q $1`
if [[ $? != 0 ]]; then
  echo "ping failed"
  exit 1
fi

function report {

  now=`date +%s%N`

  curl \
    -i -XPOST "$url/write?db=$DB" \
    --data-binary \
    "ping,from=$me,to=$target min=$1,avg=$2,max=$3,mdev=$4"

}

rx="rtt min/avg/max/mdev = ([0-9]+\.[0-9]+)/([0-9]+\.[0-9]+)/([0-9]+\.[0-9]+)/([0-9]+\.[0-9]+)"

if [[ $out =~ $rx ]]; then

  #echo "${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},${BASH_REMATCH[4]}"
  report ${BASH_REMATCH[1]} ${BASH_REMATCH[2]} ${BASH_REMATCH[3]} ${BASH_REMATCH[4]}

else
  echo "unexpected output '$out'"
  exit 1
fi

