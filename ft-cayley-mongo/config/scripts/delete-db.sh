#!/bin/bash

SERVER=${SERVER:=localhost}

if [[ $# -ne 1 ]]; then
  echo "usage: delete-db.sh <name>"
  exit 1
fi

curl -i -XPOST http://$SERVER:8086/query --data-urlencode "q=DROP DATABASE $1"

