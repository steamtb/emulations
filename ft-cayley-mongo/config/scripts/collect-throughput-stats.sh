#!/bin/bash

SERVER=${SERVER:=localhost}

if [[ $# -ne 1 ]]; then
  echo "usage: collect-ping-stats.sh <db>"
  exit 1
fi

curl \
  -G "http://$SERVER:8086/query?pretty=true" \
  --data-urlencode "db=$1" \
  --data-urlencode "q=SELECT * FROM \"iperf\""
