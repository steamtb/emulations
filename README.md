# emulations

This repository conatins system emulation models for the SteamTB project. Here is a list of the current models

- [ft-cayley-mongo](ft-cayley-mongo): An emulation of a fattree system hosting a cayley graph implementation backed by a sharded MongoDB data store.
