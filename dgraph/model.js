/* ============================================================================
 *  Cloud node types
 * ==========================================================================*/

/* AWS -----------------------------------------------------------------------
 * ===
 *
 * https://aws.amazon.com/ec2/instance-types/
 *
 */

if(env.FREEBASE === null) {
  console.log(
    'you must set the FREEBASE environment varible to point to a directory\n'+
    'containing the freebase data, with the filename freebase-rdf-latest.gz\n'+
    'rebuild the topology after setting.'
  );
  process.exit(1);
}

var aws = {
  m4: {
    large: (name) => ({
      name: name,
      image: 'fedora-28', 
      cpu: { cores: 2 },
      memory: { capacity: GB(8) }
    }),
    xlarge: (name) => ({
      name: name,
      image: 'fedora-28', 
      cpu: { cores: 4 },
      memory: { capacity: GB(16) }
    })
  }
};

function cumulus(name) {
  return {
    'name': name,
    'image': 'cumulusvx-3.5-mvrf',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(1) }
  };
}

/* AWS recommends m4.large for mongo */
var nodes = Range(4).map(i => {
  let n = aws.m4.xlarge('n'+i);
  n['mounts'] = [{ source: env.FREEBASE, point: '/tmp/freebase' }];
  return n;
});
var sw = cumulus('sw');
var zero = aws.m4.xlarge('zero');
zero['mounts'] = [{ source: env.FREEBASE, point: '/tmp/freebase' }];

var switch_port = 1;
var topo = {
  name: 'dgraph',
  nodes: [...nodes, zero],
  switches: [sw],
  links: [
    ...nodes.map(x => Link(x.name, 1, 'sw', switch_port++)),
    Link('zero', 1, 'sw', switch_port++)
  ],
};
